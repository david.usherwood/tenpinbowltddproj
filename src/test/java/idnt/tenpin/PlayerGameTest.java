package idnt.tenpin;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PlayerGameTest {

  @Mock PlayerHistory playerHistory;
  @InjectMocks
  private static PlayerGame playerGame;
  @BeforeEach
  public void setup() {

    playerGame = new PlayerGame(playerHistory);

  }

    private static int playerGame(PlayerHistory playerHistory, Integer[] game) {
      when(playerHistory.getRolls()).thenReturn(game);
      return playerGame.score(playerHistory.getRolls());
    }

    @ParameterizedTest
    @MethodSource("regularGame")
    public void testingScoreForRegularGame(int score, Integer[] game) {
        int result = playerGame(playerHistory, game);
        assertEquals(score, result);
    }
    @Test
   public void testAllStrikes(){
      int result = playerGame(playerHistory, GAME_WITH_ALL_STRIKES);
      assertEquals(300, result);

   }

  @Test
  public void testGameWithEmptyRolls(){
    int result = playerGame.score(REGULAR_GAME_NO_HITS);
    assertEquals(0,result);
  }

  @ParameterizedTest
  @MethodSource("gameWithSpares")
  @DisplayName("Test game with spares not in final frame")
  public void testGameWithSparesNotInFinalFrame(int score, Integer[] game){
    int result = playerGame.score(game);
    assertEquals(score,result);
  }

  @Test
  @DisplayName("Test game with spares in final frame")
  public void testGameWithSparesInFinalFrame(){
    int result = playerGame.score(SPARE_IN_FINAL_FRAME);
    assertEquals(11,result);
  }

  private static Stream<Arguments> regularGame() {
    return Stream.of(
        Arguments.of(64,REGULAR_GAME_1),
        Arguments.of( 59,REGULAR_GAME_2),
            Arguments.of( 48,REGULAR_GAME_3),
            Arguments.of( 60,REGULAR_GAME_4));
  }

  private static Stream<Arguments> gameWithSpares() {
    return Stream.of(
        Arguments.of(27, REGULAR_GAME_WITH_ONE_SPARE),
        Arguments.of(45,REGULAR_GAME_WITH_THREE_SPARE));
  }


  static Integer [] REGULAR_GAME_NO_HITS =
      {0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0};

  static Integer [] SPARE_IN_FINAL_FRAME =
      {0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0  ,6,4,1};

  static Integer [] GAME_WITH_ALL_STRIKES =
      {10,10,10,10,10,10,10,10,10,10,  10, 10};
  static Integer [] REGULAR_GAME_WITH_ONE_SPARE =
          {1,1,1,9,1,1,1,1,1,1,  1,1,1,1,1,1,1,1,0,0};
    static Integer [] REGULAR_GAME_WITH_THREE_SPARE =
            {1,1,1,9,1,9,1,1,1,1,  1,1,1,9,1,1,1,1,0,0};
  static Integer [] REGULAR_GAME_1 =
      {1,3,4,2,6,1,3,5,5,2,  1,3,4,2,6,1,3,5,5,2};
  static Integer [] REGULAR_GAME_2 =
      {1,3,4,2,1,1,3,5,5,2,  1,3,4,2,6,1,3,5,5,2};
  static Integer [] REGULAR_GAME_3 =
      {10,3,4,2,1,1,3,0,0,2,  1,3,4,2,1,1,3,0,0};

  static Integer [] REGULAR_GAME_4 =
      {10,3,4,2,1,1,3,0,0,2,  1,3,4,2,1,1,3,0,10,2};
}
