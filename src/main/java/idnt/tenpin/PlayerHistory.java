package idnt.tenpin;

import java.util.*;

public class PlayerHistory {
    private final List<Integer> rollHistory = new ArrayList<>();

    public void addRoll(final int pins) {
        rollHistory.add(pins);
    }

    public Integer[] getRolls() {
        return rollHistory.toArray(new Integer[0]);
    }


}
