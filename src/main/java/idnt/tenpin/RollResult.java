package idnt.tenpin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter

public class RollResult {

  private boolean success = false;
  private int rollNo = 1;
  private int frame = 1;
  private int currrentNoOfPins = 10;

}
