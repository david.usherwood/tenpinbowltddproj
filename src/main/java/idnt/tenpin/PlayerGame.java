package idnt.tenpin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PlayerGame {

  private static final int MIN_PINS = 0;
  private static final int MAX_PIN = 10;
  public static final int MAX_FRAME_SIZE = 2;
  private static final int MAX_FRAMES = 10;
  private static final int STRIKE_OR_SPARE_POINTS = 10;
  private static final int STRIKE = 10;
  private static final int SPARE = 10;

  public enum StateEnum {
    STARTFRAMESTATE("STATE3"),

    MIDFRAMESTATE("STATE1"),
   MIDFINALFRAMESTATE("STATE5"),

    FINALFRAMESTRIKE("STATE6"),

    STARTFINALFRAME("STATE4"),
    ENDFINALFRAME("STATE7"),
    FINALFRAMEBONUS("STATE8"),
    ENDFRAMESTATE("STATE2"),

    ENDOFGAMESTATE("STATE9");
    private final String label;

    StateEnum(final String enumLabel) {
      this.label = enumLabel;
    }

    public String getLabel() {
      return label;
    }
  }

  private int nxtFrameNo = 1;

  private int noOfRolls = 0;

  private PlayerHistory playerHistory;

  public PlayerGame(final PlayerHistory gameHistory) {
    playerHistory = gameHistory;
  }

  /***
   * Calculates the final score.
   * @param rollHistory Integer[] An array of Players rolls
   * @return final score
   */
  public int score(final Integer[] rollHistory) {
    int result = 0;
    int index = 0;
    StateEnum state = StateEnum.STARTFRAMESTATE;
    int frame = 1;
    int pins = 10;

    displayHist(rollHistory);

    while (state != StateEnum.ENDOFGAMESTATE) {
      int roll = rollHistory[index];
      displayLogs(state, pins, roll, frame, index, result);
      switch (state) {
        case MIDFRAMESTATE:
          result += roll;
          index++;
          state = StateEnum.ENDFRAMESTATE;
          break;
        case ENDFRAMESTATE:
          index++;
          if (rollHistory[index - 1] == 10) {
            result += rollHistory[index] + rollHistory[index + 1];
          } else if (rollHistory[index - 1] + rollHistory[index - 2] == 10) {
            result += rollHistory[index];
          }
          result += roll;
          frame = frame + 1;
          if (frame < 10) {
            state = StateEnum.STARTFRAMESTATE;
          } else {
            state = StateEnum.STARTFINALFRAME;
          }
          break;
        case STARTFRAMESTATE:
          if (roll >= 0 && roll <= 9) {
            state = StateEnum.MIDFRAMESTATE;
          } else {
            state = StateEnum.ENDFRAMESTATE;
          }
          break;
        case STARTFINALFRAME:
          result += roll;
          pins -= roll;
          index++;
          if (roll < 10) {
            state = StateEnum.MIDFINALFRAMESTATE;
          } else {
            state = StateEnum.FINALFRAMESTRIKE;
          }
          break;
        case MIDFINALFRAMESTATE:
          pins -= roll;
          state = StateEnum.ENDFINALFRAME;
          break;
        case FINALFRAMESTRIKE:
          result += roll;
          state = StateEnum.FINALFRAMEBONUS;
          break;
        case ENDFINALFRAME:
          result += roll;
          index++;
          if (pins != 0) {
            state = StateEnum.ENDOFGAMESTATE;
            break;
          } else {
            pins = 10;
            state = StateEnum.FINALFRAMEBONUS;
          }
          break;
        case FINALFRAMEBONUS:
          result += roll;
          state = StateEnum.ENDOFGAMESTATE;
          break;
        default:
          break;
      }
    }
    return result;
  }

  private void displayLogs(final StateEnum state, final int pins,
      final int roll, final int frame, final int index, final int result) {

    System.out.printf("State %s, pins %d, roll %d, Frame %d, "
            + "Index %d, result %d\n", state.getLabel(), pins,
        roll, frame, index, result);
  }

  private void displayHist(final Integer[] rollHistory) {
    StringBuilder stringBuilder = new StringBuilder();

    for (Integer integer : rollHistory) {

      stringBuilder.append(integer).append(", ");
    }
    System.out.println("RollHist " + stringBuilder + "\n");
  }
}




